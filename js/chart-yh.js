function showGauge() {
  var chart = echarts.init(document.getElementById('gauge'));
  chart.setOption(
    {
      series : [
        {
          name:'',
          type:'gauge',
          radius:'100%',
          detail : {
            formatter:'{value}%',
            fontSize:'12'
          },
          data:[{value: 100, name: '隐患整改率'}  ],
          axisTick:{
            length:3
          },
          axisLabel:{
            distance:-20
          },
          //刻度线
          axisLine: {
            show: false,
            distance: 10,
            lineStyle: {
              width: 8,
              color: [
                [0.2, '#E51C23'],
                [0.8, '#F5A623'],
                [1, '#0D80FE']
              ]
            }
          },
          splitLine:{
            show:false
          },
          pointer:{
            width:2
          },
          title:{
            color:'#63869E',
            fontSize:12
          }
        }
      ]
    }
  )
  $(window).resize(function(){
    chart.resize();
    }
  )
}

function showPie1 () {
  var chart = echarts.init(document.getElementById('pie1'));
  chart.setOption(
    {
      color: [
        '#FF5722',
        '#0D80FE'
      ],
      tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
      },
      series: [
        {
          name: '隐患来源情况',
          type: 'pie',
          radius: [30, 60],
          center: ['50%', '50%'],
          label: {
            normal: {
              show: true,
              position: 'outside',
              formatter: '{b}，{c}\n{d}%'
            }
          },
          itemStyle:{
            normal:{
              borderColor:'#fff',
              borderWidth:'1'
            }
          },
          data: [
            {value: 10, name: '政府排查'},
            {value: 5, name: '企业自查'}
          ]
        }
      ]
    }
  )
  $(window).resize(function(){
      chart.resize();
    }
  )
}

function showBar3 (data) {
  var chart = echarts.init(document.getElementById('bar3'));
  chart.setOption(
    {
      color: ['#F5A623'],
      grid: {
        top: '10',
        bottom: '20',
        left:'5%',
        right:'8%',
        containLabel: true
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {            // 坐标轴指示器，坐标轴触发有效
          type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
      },
      xAxis: [
        {
          axisLine: {
            lineStyle:{
              color:'#1178C9'
            }
          },
          axisLabel:{
            interval:0,
            color:'#607D8B'
          },
          splitLine:{
            show:false
          },
          type: 'value',
          splitArea: {
            interval: 'auto',
            show: true,
            areaStyle: {
              color: ['#F8F8F8', '#fff']
            }
          }
        }
      ],
      yAxis: [
        {
          type: 'category',
          axisLine: {
            lineStyle:{
              color:'#1178C9'
            }
          },
          inverse:true,
          axisLabel:{
            color:'#607D8B',
            formatter:function (value) {
              if (value.length >7){
                return value.substr(0,7)+'\n'+value.substr(7)
              }else{
                return value.substr(0,7)
              }
            }
          },
          axisTick: {
            show: false
          },
          data:data.y
        }
      ],
      series: [
        {
          type: 'bar',
          barWidth: '45%',
          label: {
            normal: {
              show: true,
              position: 'right',
              color: '#999'
            }
          },
          data:data.data
        }
      ]
    }
  )
  return chart;
  $(window).resize(function(){
      chart.resize();
    }
  )
}

//各月隐患治理情况统计
function showBar1 (data) {
  var chart = echarts.init(document.getElementById('bar1'))

  chart.setOption(
    {
      color: [
        '#0098FF',
        '#FF9800',
        '#E51C23',
        '#F1421E'
      ],
      legend: {
        right: '100',
        top: '10',
        itemWidth: 14,
        itemHeight: 8,
        data: ['排查隐患数', '已整改', '超期未整改','整改率'],
        textStyle: {
          color: '#6F7B8D'
        }
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {            // 坐标轴指示器，坐标轴触发有效
          type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        },
        formatter: function (params) {
          var res = ''
          for (var i = 0; i < params.length; i++) {
            var param = params[i]
            if (param.seriesName == '整改率') {
              var item = param.seriesName + ':' + param.data + '%'
            } else {
              var item = param.seriesName + ':' + param.data
            }
            res += '' + item + '</br>'
          }

          return res
        }
      },
      grid: {
        top: '25%',
        left: '3%',
        right: '3%',
        bottom: '18%',
        containLabel: true
      },
      dataZoom: [
        {
          show: true,
          bottom:'8',
          start: 0,
          end: 70,
          height:'22',

        }

      ],
      xAxis: [
        {
          type: 'category',
          data: data.x,
          splitLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisLine: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            interval: 0,
            textStyle: {
              color: '#607D8B'

            }
          }
        }
      ],
      yAxis: [
        {
          type: 'value',
          axisLabel: {
            textStyle: {
              color: '#607D8B'
            }
          },
          axisLine: {
            show: true,
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          splitLine: {
            show: false,
            lineStyle: {
              color: '#07274D'
            }
          },
          splitArea: {
            show: true,
            areaStyle: {
              color: ['#F8F8F8', '#fff']
            }
          }
        },
        {
          type: 'value',
          max: 100,
          min: 0,
          axisLabel: {
            textStyle: {
              color: '#97B8C9'

            },
            formatter: function (v) {
              return v + '%'
            }
          },
          axisLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          splitLine: {
            show: false,
          }
        }
      ],
      series: [
        {
          name: '排查隐患数',
          type: 'bar',
          yAxisIndex: 0,
          data: data.series[0].data
        },
        {
          name: '已整改',
          type: 'bar',
          yAxisIndex: 0,
          data: data.series[1].data
        },
        {
          name: '超期未整改',
          type: 'bar',
          yAxisIndex: 0,
          data: data.series[2].data
        },
        {
          name: '整改率',
          type: 'line',
          yAxisIndex: 1,
          data: data.series[3].data
        }
      ]

    }
  )
  $(window).resize(function () {
      chart.resize()
    }
  )
  return chart
}

//各行业企业隐患分布情况 切换
function showBar2 (data) {

  var chart = echarts.init(document.getElementById('bar2'))
  var option = {
    baseOption: {
      animationDurationUpdate: 1000,
      animationEasingUpdate: 'quinticInOut',
      timeline: {
        axisType: 'category',
        orient: 'vertical',
        autoPlay: true,
        inverse: true,
        playInterval: 10000,
        right: 15,
        top: 40,
        bottom: 20,
        width: 2,
        label: {
          normal: {
            textStyle: {
              color: '#0D80FE',
              padding: 5
            },
            position: 'right'
          },
          emphasis: {
            textStyle: {
              color: '#4BCEEC'
            }
          }
        },
        symbol: 'image://./images/time-line-icon.png',

        lineStyle: {
          width: 1,
          color: '#0D80FE',
          type: 'dotted'
        },
        checkpointStyle: {
          symbol: 'image://./images/checkpoint-style-icon.png'
        },
        controlStyle: {
          show: false
        },
        data: ['各行业隐患\n分布情况', '各地区隐患\n来源分布']
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {            // 坐标轴指示器，坐标轴触发有效
          type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        },
      },
      xAxis: [
        {
          type: 'category',
          data: data.x,
          splitLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisLine: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            interval: 0,
            textStyle: {
              color: '#607D8B'
            }
          }
        }
      ],
      yAxis: [
        {
          type: 'value',
          axisLabel: {
            textStyle: {
              color: '#607D8B'
            }
          },
          axisLine: {
            show: true,
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          splitLine: {
            show: false,
            lineStyle: {
              color: '#07274D'
            }
          },
          splitArea: {
            show: true,
            areaStyle: {
              color: ['#F8F8F8', '#fff']
            }
          }
        }
      ],
      // dataZoom: [
      //   {
      //     show: true,
      //     bottom:'8',
      //     start: 0,
      //     end: 70,
      //     height:'22',
      //
      //   }
      //
      // ],
      grid:{
        top:'20%',
        bottom:'15%',
        left:'8%',
        right:'15%'
      },
      legend: {
        right: '150',
        top: '10',
        itemWidth: 14,
        itemHeight: 8,
        textStyle: {
          color: '#6F7B8D'
        }
      },
    },
    options: [{
      legend: {
        show:false,
        data:[]
      },
      series:[
        {
          type: 'bar',
          name:'',
          barWidth: '50%',
          itemStyle: {
            normal: {
              color: '#0098FF'
            }
          },
          data:data.hy.data
        },
        {
          name: '',
          type: 'bar',
          data: []
        }

      ],
      xAxis: [
        {
          data: data.hy.x
        }
      ]
    },
      {
        legend: {
          show:true,

          data: ['政府排查', '企业自查']

        },
        series:[
          {
            name: '政府排查',
            type: 'bar',
            barWidth: '',
            itemStyle: {
              normal: {
                color: '#EB3432'
              }
            },
            data: data.dq.data[0]
          },
          {
            name: '企业自查',
            type: 'bar',
            itemStyle: {
              normal: {
                color: '#0099FF'
              }
            },
            data: data.dq.data[1]
          }

        ],
        xAxis: [
          {
            data: data.dq.x
          }
        ]
      },

    ]
  }

  chart.setOption(option)
  chart.on('timelinechanged', function (curr) {
    var currIdx = curr.currentIndex;
    $('#bottom_right_title').html('各行业隐患分布情况');
    if (currIdx == 0){
      $('#bottom_right_title').html('各地区隐患 来源分布')
    }
  })
  return chart;

}




function showMap (mapData, code) {
  var chart = echarts.init(document.getElementById('map'));
  $.get('./map/' + code + '.json', function (json) {
    echarts.registerMap(code, json);
    var option =  {
      tooltip: {
        trigger: 'item',
        formatter: function(params) {
          if (params.seriesIndex == 0){
            return params.data.fromName +':' +params.data.value+  ' > ' + params.data.toName
          }
        }
      },
      geo:{
        map:code,
        roam:true,
        zoom:1.2,
        itemStyle: {
          normal: {
            borderColor: '#64B6FE',
            borderWidth: 1.5,
            areaColor: '#D2F1FE'
          },
          emphasis: {
            // areaColor: '#FEFEFE'
          }
        },
        label:{
          normal:{
            show:true,
            color:'#1178c9'
          },
          emphasis:{
            show:false,
            // color:'#CCCCCC',
          }
        }
      },
      visualMap:[
        {
          color:['#FF4B07','#FDA88E'],
          min : 0,
          max : 100,
          range:[0,100],
          calculable:true,
          text: ['高', '低'],
          left:20,
          bottom:10,
          textStyle:{
            color:'#FF5722'
          },
          seriesIndex:0

        }
      ],

      series : [
        {
          type: 'lines',
          zlevel: 1,

          effect: {
            show: true,
            period: 6,
            trailLength: 0,
            symbolSize: 5,
            shadowColor: 'rgba(0, 0, 0, 0.5)',
            shadowBlur:10
          },
          symbol: [
            'circle', 'arrow'
          ],
          lineStyle: {
            normal: {
              width: 1,
              curveness: 0.2
            }
          },
          label:{
            normal:{
              show:true,
              position:'start',
              shadowOffsetX:10,
              shadowOffsetY:10
            }
          },
          data:mapData.lines
        },
        {
          name: '',
          type: 'effectScatter',
          coordinateSystem: 'geo',
          symbolSize: 10,
          showEffectOn: 'render',
          rippleEffect: {
            brushType: 'stroke'
          },
          hoverAnimation: true,
          label: {
            normal: {
              show: false
            }
          },
          itemStyle: {
            normal: {
              color: '#0D80FE',
              shadowBlur: 10,
              shadowColor: '#333'
            }
          },
          zlevel: 2,
          data:mapData.center
        }

      ]
    };
    chart.setOption(option)
  })
return chart;
}





