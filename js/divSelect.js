jQuery.divselect = function (divselectid, inputselectid) {
    var inputselect = $(inputselectid);
    var cite = $(divselectid + " cite");
    $(divselectid + " cite").click(function () {
        var ul = $(divselectid + " ul");
        if (ul.css("display") == "none") {
            ul.slideDown("fast");
            cite.addClass("active")
            ul.addClass("active")
        } else {
            ul.slideUp("fast");
            cite.removeClass("active");
            ul.removeClass("active");

        }
    });
    $(divselectid + " ul li a").click(function () {
        var txt = $(this).text();
        $(divselectid + " cite").html(txt);
        var value = $(this).attr("val");
      
        inputselect.val(value);
        $(divselectid + " ul").hide();
        cite.removeClass("active");

    });
    $(document).on('click', function (e) {
        var target = $(e.target);
        if (target.closest('.select').length === 0) {
            $(divselectid + " ul").hide();
            cite.removeClass("active");
        } else {
            return false;
        }
    })


};
