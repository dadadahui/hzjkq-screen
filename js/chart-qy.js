//行业类别
function getColorByCodeOrName (code, name) {
  if (code == 'SYS1601' || name == '危险化学品') {
    return '#FF4321'
  } else if (code == 'SYS1602' || name == '烟花爆竹') {
    return '#B91D1C'
  } else if (code == 'SYS1603' || name == '非煤矿山') {
    return '#5C5F6B'

  }  else if (code == 'SYS1604' || name == '综合') {
    return '#741DFF'

  } else if (code == 'SYS1605' || name == '商贸行业') {
    return '#27972E'

  } else if (code == 'SYS1606' || name == '食品生产') {
    return '#F6C317'

  }else {
    return '#018DD3'
  }
}

function getVisualMapColorByCodeOrName (code, name) {
  if (code == 'SYS1601' || name == '危险化学品') {
    return ['#E93414', '#FCDBD1']
  } else if (code == 'SYS1602' || name == '烟花爆竹') {
    return ['#B91D1C', '#F4D6D3']
  } else if (code == 'SYS1603' || name == '非煤矿山') {
    return ['#474646', '#D2D2D2']
  } else if (code == 'SYS1604' || name == '综合') {
    return ['#741DFF', '#E7D9FF']

  } else if (code == 'SYS1605' || name == '商贸行业') {
    return ['#2BA43C', '#D5EDD8']

  } else if (code == 'SYS1606' || name == '食品生产') {
    return ['#F6C317', '#FEF5D1']

  } else {
    return ['#018DD3', '#DDEDFE']
  }
}
// 企业行业分布情况
function showPie1 (data) {
  var chart = echarts.init(document.getElementById('pie1'))

  chart.setOption(
    {
      tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b} : {c} ({d}%)'
      },
      series: [
        {
          name: '企业行业分布情况',
          type: 'pie',
          radius: [25, 45],
          center: ['50%', '50%'],
          label: {
            normal: {
              show: true,
              position: 'outside',
              formatter: '{b}，{c}\n{d}%'
            }
          },
          labelLine: {
            normal: {
              length: 2,
              length1: 2
            }

          },
          itemStyle: {
            normal: {
              borderColor: '#fff',
              borderWidth: '1',
              color: function (value) {
                return getColorByCodeOrName(value.data.code)
              }
            }
          },
          data: data
        }
      ]
    }
  )

  $(window).resize(function () {
      chart.resize()
    }
  )
  return chart
}
function showBar1 (data, pcode) {
  var chart = echarts.init(document.getElementById('bar1'))

  chart.setOption(
    {

      tooltip: {
        trigger: 'axis',
        axisPointer: {            // 坐标轴指示器，坐标轴触发有效
          type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
      },
      grid: {
        top: '10%',
        left: '3%',
        right: '4%',
        bottom: '30%',
        containLabel: true
      },
      dataZoom: [
        {
          show: true,
          bottom: '18%',
          startValue: 0,
          endValue: 4,
          // end: 70,
          height: '22',

        }

      ],

      xAxis: [
        {
          type: 'category',
          data: data.x,

          axisLine: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            interval: 0,
            textStyle: {
              color: '#607D8B'
            },
            formatter: function (val) {
              return val.substr(val,4)+'\n'+val.substring(4,7)+'...';
            }
          }
        }
      ],
      yAxis: [
        {
          type: 'value',
          axisLine: {
            show: true,
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            interval: 0,
            textStyle: {
              color: '#607D8B'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          splitLine: {
            show:false,
          },
          splitArea: {
            show: true,
            areaStyle: {
              color: ['#F8F8F8', '#fff']
            }
          }
        }
      ],
      series: [
        {
          name: '行政许可持证情况',
          type: 'bar',
          barWidth: '50%',
          data: data.data,
          itemStyle: {
            normal: {
              color: function () {
                return getColorByCodeOrName(pcode)
              }
            }
          }
        }

      ]

    }
  )
  $(window).resize(function () {
      chart.resize()
    }
  )
  return chart
}

function showBar2 () {
  var chart = echarts.init(document.getElementById('bar2'))

  chart.setOption(
    {
      color: ['#05D187'],
      tooltip: {
        trigger: 'axis',
        axisPointer: {            // 坐标轴指示器，坐标轴触发有效
          type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
      },
      grid: {
        top: '15%',
        left: '3%',
        right: '4%',
        bottom: '8%',
        containLabel: true
      },
      // dataZoom: [
      //   {
      //     show: true,
      //     type: 'slider',
      //     start: 0,
      //     end: 40,
      //   },
      // ],
      xAxis: [
        {
          type: 'category',
          data: [
            '有限空间作业场所',
            '涉及可燃爆粉尘作业场所',
            '喷涂作业场所',
            '船舶修造企业',
            '涉氨制冷企业'
           ] ,
          axisLine: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            interval: 0,
            color: '#686868',
            formatter:function (v) {
              return v.substring(0,6)+'\n'+v.substring(6)
            }
          }
        }
      ],
      yAxis: [
        {
          type: 'value',
          splitLine: {
            show: false
          },
          splitArea: {
            interval: 'auto',
            show: true,
            areaStyle: {
              color: ['#F8F8F8', '#fff']
            }
          },
          axisLine: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            interval: 0,
            color: '#686868'
          }
        }
      ],
      series: [
        {
          name: '',
          type: 'bar',
          barWidth: '60%',
          data: [10, 52, 200, 334, 390, 330, 220]
        }
      ]

    }
  )
  $(window).resize(function () {
      chart.resize()
    }
  )
}
function showBar3 () {
  var chart = echarts.init(document.getElementById('bar3'))

  chart.setOption(
    {
      color: ['#0098FF',

      ],

      tooltip: {
        trigger: 'axis',
        axisPointer: {            // 坐标轴指示器，坐标轴触发有效
          type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
      },
      grid: {
        top: '15%',
        left: '3%',
        right: '4%',
        bottom: '5%',
        containLabel: true
      },
      xAxis: [
        {
          type: 'category',
          data: ['宝鸡市', '宝鸡市', '宝鸡市', '宝鸡市', '宝鸡市', '宝鸡市', '宝鸡市'],
          axisLine: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            interval: 0,
            color: '#686868'
          }
        }
      ],
      yAxis: [
        {
          type: 'value',
          splitLine: {
            show: false
          },
          splitArea: {
            interval: 'auto',
            show: true,
            areaStyle: {
              color: ['#F8F8F8', '#fff']
            }
          },
          axisLine: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            color: '#686868'
          }
        }
      ],
      series: [
        {
          name: '',
          type: 'bar',
          data: [10, 52, 200, 334, 390, 330, 220]
        },

      ]

    }
  )
  $(window).resize(function () {
      chart.resize()
    }
  )
}

//企业分级情况
function showBar4 (data) {

  var chart = echarts.init(document.getElementById('bar4'))
  var option = {
    baseOption: {
      animationDurationUpdate: 1000,
      animationEasingUpdate: 'quinticInOut',
      timeline: {
        axisType: 'category',
        left: '15%',
        right: '15%',
        top:'10',
        autoPlay: true,
        playInterval: 15000,
        label: {
          normal: {
            textStyle: {
              color: '#0D80FE',
              padding: 10
            },
            position: 'bottom'
          },
          emphasis: {
            textStyle: {
              color: '#4BCEEC'
            }
          }
        },
        symbol: 'image://./images/time-line-icon.png',

        lineStyle: {
          width: 1,
          color: '#0D80FE',
          type: 'dotted'
        },
        checkpointStyle: {
          symbol: 'image://./images/checkpoint-style-icon.png'
        },
        controlStyle: {
          show: false
        },
        data: ['规模情况', '标准化等级']
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {            // 坐标轴指示器，坐标轴触发有效
          type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
      },
      xAxis: [
        {
          type: 'category',
          data: data.x,

          axisLine: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            interval: 0,
            textStyle: {
              color: '#607D8B'
            }
          }
        }
      ],
      yAxis: [
        {
          type: 'value',
          axisLabel: {
            textStyle: {
              color: '#607D8B'
            }
          },
          axisLine: {
            show: true,
            lineStyle: {
              color: '#1178c9'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          splitLine: {
            show: false,
            lineStyle: {
              color: '#07274D'
            }
          },
          splitArea: {
            show: true,
            areaStyle: {
              color: ['#F8F8F8', '#fff']
            }
          }
        }
      ],
      grid:{
        top:'45%',
        bottom:'15%',
        left:'15%'
      },
      series:[{
        type: 'bar',
        itemStyle: {
          normal: {
            color: function(value) {
              var colorList = [
                '#05D187',
                '#FF7200',
                '#FAB034',
                '#29A1F7'
              ]
              return colorList[value.dataIndex]

            }
          }
        },
      }]
    },
    options: [{
      series:[
        {
          name: '',
          type: 'bar',
          barWidth: '50%',
          data: data.gm.data
        }
      ],
      xAxis: [
        {
          data: data.gm.x
        }
      ]
    },
      {
        series:[
          {
            name: '',
            type: 'bar',
            barWidth: '50%',
            data: data.bzh.data
          }
        ],
        xAxis: [
          {
            data: data.bzh.x
          }
        ]
      }
    ]
  }

  chart.setOption(option)




}


function showMap (mapdata, max, code) {
  var chart = echarts.init(document.getElementById('map'))

  function getSerieTotal (data) {
    var total = 0
    for (var i = 0; i < data.length; i++) {
      total += data[i].value
    }
    return total
  }

  $.get('./map/' + code + '.json', function (json) {

    echarts.registerMap(code, json)
    var option = {

      tooltip: {
        trigger: 'item'
      },
      legend: {
        selectedMode: 'single',
        orient: 'vertical',
        itemWidth: 12,
        itemHeight: 12,
        x: '10',
        y: '20',
        icon: 'circle',
        data: [],
      },
      series: [
      ],
      visualMap: {
        min: 0,
        max: max,
        left: '20',
        top: 'bottom',
        text: ['高', '低'],           // 文本，默认为数值文本
        color: ['#018DD3', '#DDEDFE'],
        dimension:0,
        calculable: true
      }
    }
    for (var i = 0; i < mapdata.length; i++) {
      var industryData = mapdata[i]
      var serie = {
        name: function () {
          var data = mapdata[i].data
          return industryData.industry + '(' + getSerieTotal(data) + ')'
        }(),
        roam: true,
        type: 'map',
        zoom: 1.2,
        map: code,
        label: {
          normal: {
            show: true,
            color: '#006ACC'
          }
        },
        itemStyle: {
          normal: {
            borderColor: '#64B6FE',
            borderWidth: 1.5,
            // areaColor: '#D1F1FF',
            color: getColorByCodeOrName(mapdata[i].code)

          }
        },
        data: industryData.data
      }
      option.series.push(serie)
      if (i == 0) {
        option.legend.data.push({
          name: industryData.industry + '(' + getSerieTotal(option.series[i].data) + ')',
          textStyle: {
            color: '#018DD3'
          }
        })
      }
      else {
        option.legend.data.push({
          name: industryData.industry + '(' + getSerieTotal(option.series[i].data) + ')',
          textStyle: {
            color: getColorByCodeOrName(mapdata[i].code)
          }
        })
      }

    }
    chart.setOption(option)
  })
  return chart
}
