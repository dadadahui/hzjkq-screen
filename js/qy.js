$(function () {
  var pie1Data = [
    {value: 100, name: '危险化学品',code:'SYS1601'},
    {value: 1, name: '烟花爆竹',code:'SYS1602'},
    {value: 15, name: '非煤矿山',code:'SYS1603'},
    {value: 15, name: '综合',code:'SYS1604'},
    {value: 15, name: '商贸行业',code:'SYS1605'},
    {value: 15, name: '食品生产',code:'SYS1606'}
  ]
  var pie1 = showPie1(pie1Data);

  pie1.on('click',function (param) {
    var pcode = param.data.code;
    var bar1data = {
      x: [
        '危险化学品经营许可证', '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）',
        '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）',
        '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）'
      ],
      data: [802, 52, 66, 334, 390, 330, 220]
    }
    showBar1(bar1data,pcode)
  })

  var bar1data = {
    x: [
      '危化生产(石油加工、炼焦和核燃料制造)', '危化生产(化学原料和化学制品制造)', '危化生产(医药制造)',
      '危险化学品经营（有储存）', '危险化学品经营（无储存）',
      '危险化品使用（取证）'
    ],
    data: [10, 52, 66, 334, 390, 330, 220]
  }
  showBar1(bar1data,'SYS1601');



  showBar2();
  showBar3();
  var bar4Data = {
    gm:
      {
        x: ['规模以上 ','规模以下','小微'],
        data: [1,1,1]
      },
    bzh: {
      x: ['一级 ','二级','三级','规范化企业'],
      data: [1,2,1,1]
    }

  }
  showBar4(bar4Data)

  var mapData = [
    {
      'industry':'生产经营单位总数',
      'code':'0',
      'data':[
        {
          'name':'杨家埠街道',
          'value':111,

        },
        {
          'name': '龙溪街道',
          'value': 300,
        }
    ]
    },
    {
      'industry':'非煤矿山',
      'code':'SYS1603',
      'data':[
        {
          'name':'杨家埠街道',
          'value':111,

        },
        {
          'name': '龙溪街道',
          'value': 300,
        }
      ]
    },
    {
      'industry':'危险化学品',
      'code':'SYS1601',
      'data':[
        {
          'name':'杨家埠街道',
          'value':111,

        },
        {
          'name': '龙溪街道',
          'value': 300,
        }
      ]
    },
    {
      'industry':'烟花爆竹',
      'code':'SYS1602',
      'data':[
        {
          'name':'杨家埠街道',
          'value':111,

        },
        {
          'name': '龙溪街道',
          'value': 300,
        }
      ]
    },
    {
      'industry':'商贸行业',
      'code':'SYS1605',
      'data':[
        {
          'name':'杨家埠街道',
          'value':111,

        },
        {
          'name': '龙溪街道',
          'value': 300,
        }
      ]
    },
    {
      'industry':'食品生产',
      'code':'SYS1606',
      'data':[
        {
          'name':'杨家埠街道',
          'value':111,

        },
        {
          'name': '龙溪街道',
          'value': 300,
        }
      ]
    },
    {
      'industry':'综合',
      'code':'SYS1604',
      'data':[
        {
          'name':'杨家埠街道',
          'value':111,

        },
        {
          'name': '龙溪街道',
          'value': 300,
        }
      ]
    }
  ];


  var chartMap = showMap(mapData,1000,'330560000');


  chartMap.on('legendselectchanged',function (e) {
    var selectedObj = e.selected;
    var selectName = e.name;
    var puerSelectName = e.name.slice(0, e.name.lastIndexOf('('));
    $.each(selectedObj,function (k, v) {
      if (selectedObj[selectName]){
        var option = chartMap.getOption();
        //变visualMap颜色
        option.visualMap = {
          color: getVisualMapColorByCodeOrName(undefined,puerSelectName)
        }
        chartMap.setOption(option)
        return false;
      }
    })
  })

})

function getSerieTotal (data) {
  var total = 0
  for (var i = 0; i < data.length; i++) {
    total += data[i].value
  }
  return total
}


