function wrapBy4(string) {
  return /\S{3}/.test(string)
    ? string.replace(/\s/g, '').replace(/(.{4})/g, "$1 \n")
    : string
}


// $('#navWrapper .nav').on('click',function () {
//   $(this).addClass('active').siblings('.nav').removeClass('active');
// })


